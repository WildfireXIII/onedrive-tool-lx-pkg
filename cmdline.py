import sys
import actuator as act

def displayHelp():
    print("\npossible commands:")
    print("\todrive ls [path]")
    print("\todrive up [local] [remote]")
    print("\todrive up [local] [remote] --force")
    print("\todrive down [remote]")
    print("\todrive down [remote] [local]")
    print("\todrive down [remote] [local] --force")
    
if len(sys.argv) == 1:
    displayHelp()
    exit()

operation = sys.argv[1] # 'up' or 'down' or 'ls'


if operation == "ls":
    fp1 = sys.argv[2]
    act.list(fp1)
    
elif operation == "up":
    fp1 = sys.argv[2]
    fp2 = sys.argv[3]
    force = False
    if len(sys.argv) == 5 and sys.argv[4] == "--force": force = True
    act.upload(fp1, fp2, verbose=True, force=force)

elif operation == "down":
    fp1 = sys.argv[2]
    fp2 = ""
    force = False
    if len(sys.argv) == 5:
        fp2 = sys.argv[3]
        if sys.argv[4] == "--force": force = True
    elif len(sys.argv) == 4:
        if sys.argv[3] == "--force": force = True
        else: fp2 = sys.argv[3]
    act.download(fp1, fp2, verbose=True, force=force)

else: displayHelp()
