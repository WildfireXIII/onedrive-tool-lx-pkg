import connector
import os
import datetime
import calendar
import onedrivesdk

class ItemRequestPair:
    def __init__(self, request, item):
        self.request = request
        self.item = item


def getItemByName(collection, name):
    for item in collection:
        if item.name == name:
            return item
    return None


def exists(itemRequest):
    try: 
        itemRequest.get()
        return True
    except:
        return False


def isRequestFolder(itemRequest):
    if itemRequest.get().folder == None: return False
    return True
    

def isFolder(item):
    if item.folder == None: return False
    return True


def getItemRequestByPath(path):
    return client.item(drive='me', path=path)


def getItemRequestByID(ID):
    return client.item(drive='me', id=ID)

def getSubItems(path):
    items = []
    for item in client.item(drive='me', path=path).children.get():
        items.append(ItemRequestPair(getItemRequestByID(item.id), item))
    return items

def list(path):
    items = getSubItems(path)
    for item in items:
        if isFolder(item.item):
            print("(DIR) " + item.item.name + "/")
        else:
            print(item.item.name)
            
            
        

def downloadItem(itemPair, target, force=False, verbose=False):
    # see if file should be updated by comparing time stamps
    remotetimestamp = calendar.timegm(itemPair.item.last_modified_date_time.utctimetuple())

    localtimestamp = 0
    try: localtimestamp = os.path.getmtime(itemPair.item.name)
    except: pass # file doesn't exist
    
    # only update if the difference is negative (local is older)
    dif = int(localtimestamp) - remotetimestamp 

    if dif < 0 or force:
        downloadstring = "\tDownloading " + itemPair.item.name + "..."
        if dif >= 0: downloadstring += " (WARNING - FORCING)"
        print(downloadstring)
        itemPair.request.download(target)
    else:
        if verbose: print("\tFAILED: '" + itemPair.item.name + "' - Local version newer or same, use force to override")


# if target is blank, use actual name
def download(path, target="", force=False, verbose=False):
    if target == "":
        target = path[path.rfind("/") + 1:] # (last folder name)
            
    print("Downloading '" + path + "' to '" + target + "'...")
    itemRequest = getItemRequestByPath(path)

    if isRequestFolder(itemRequest):
        # make the target if it doesn't exist
        if not os.path.exists(target):
            os.makedirs(target)

        # change to that directory
        os.chdir(target)
        
        itemPairCollection = getSubItems(path)
        for itemPair in itemPairCollection:

            if isFolder(itemPair.item):
                #print("Downloading folder " + itemPair.item.name + "...")
                download(path + "/" + itemPair.item.name, "", force, verbose)
                #print("Completed folder " + itemPair.item.name)
            else:
                downloadItem(itemPair, itemPair.item.name, force, verbose)
                
        # go back up for proper recursive purposes
        os.chdir("..")           
    else:
        itemRequest = getItemRequestByPath(path)
        downloadItem(ItemRequestPair(itemRequest, itemRequest.get()), target, force, verbose)
    print(target + " download complete")


def remoteFileTime(path):
    try:
        return calendar.timegm(getItemRequestByPath(path).get().last_modified_date_time.utctimetuple())
    except:
        return -1


def uploadItem(source, remote, force=False, verbose=False):

    # get remote file modified time
    remoteTime = remoteFileTime(remote)
    localTime = int(os.path.getmtime(source))

    if remoteTime < localTime or force:
        uploadstring = "Uploading '" + str(source) + "' to '" + str(remote) + "'..."
        
        if remoteTime >= localTime: uploadstring += " (WARNING - FORCING)"
        print(uploadstring)
        getItemRequestByPath(remote).upload(source)
        print("Uploaded!")
    else:
        if (verbose): print("\tFAILED: '" + str(source) + "' - Remote is newer or same, use force to override")
        

def upload(source, remote, force=False, verbose=False):
    # check if the source is folder or item
    if os.path.isdir(source):
        finalslash = remote.rfind("/")
        container = remote[:finalslash]
        foldername = remote[finalslash+1:]

        if container == "": container = "/"

        # if the path ended in /, then add the source inside of that folder instead of to that folder
        if foldername == "": 
            foldername = source[source.rfind("/") + 1:]
            remote += foldername

        # make sure that foldername exists inside of container, and create if it doesn't
        #if not isRequestFolder(getItemRequestByPath(remote)):
        if not exists(getItemRequestByPath(remote)):
            print("Remote path '" + remote + "' doesn't exist, creating...")
            newfolder = onedrivesdk.Item()
            newfolder.name = foldername
            newfolder.folder = onedrivesdk.Folder()
            getItemRequestByPath(container).children.add(newfolder)
            

        for item in os.listdir(source):
            upload(source + "/" + item, remote + "/" + item, force, verbose)
        
    else:
        uploadItem(source, remote, force, verbose) 
    
client = connector.loadClient()

#download("/test.txt", force=True)
#download("/test.txt")
#download("/test.txt", verbose=True)

#upload("./test.txt", "/test.txt", verbose=True)
#upload("./testfolder", "/testfolder", verbose=True)
#download("/testfolder", verbose=True)
