import os
import onedrivesdk
from onedrivesdk.helpers import GetAuthCodeServer


def loadClient():
    print("Fetching client...")
    #session_path = "./session"
    session_path = os.environ['DATA_DIR'] + "/onedrive-tool/session"

    session_found = True

    redirect_uri = 'http://localhost:8080/'
    client_secret = 'V9e5LwULoct25FGdcbW56Up'
    client_id_str = '4320f2ec-8149-4041-9890-93a48fb80d9c'
    api_base_url='https://api.onedrive.com/v1.0/'
    scopes=['wl.signin', 'wl.offline_access', 'onedrive.readwrite']

    http_provider = onedrivesdk.HttpProvider()
    auth_provider = onedrivesdk.AuthProvider(
        http_provider=http_provider,
        client_id=client_id_str,
        scopes=scopes)

    # get previous auth code
    try:
        auth_provider.load_session(path=session_path) 
        auth_provider.refresh_token()
    except:
        session_found = False

    # get the client 
    client = onedrivesdk.OneDriveClient(api_base_url, auth_provider, http_provider)

    # if we haven't authenticated on this system before, do so and save the code
    if not session_found:
        auth_url = client.auth_provider.get_auth_url(redirect_uri)
        code = GetAuthCodeServer.get_auth_code(auth_url, redirect_uri)

        auth_provider.authenticate(code, redirect_uri, client_secret)
        auth_provider.save_session(path=session_path) 

    print("Client obtained")
    return client
